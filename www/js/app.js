angular.module('DSMflickrApp', ['ionic', 'pascalprecht.translate'])
  .run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {
      if (window.cordova && window.cordova.plugins.Keyboard) {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

        // Don't remove this line unless you know what you are doing. It stops the viewport
        // from snapping when text inputs are focused. Ionic handles this internally for
        // a much nicer keyboard experience.
        cordova.plugins.Keyboard.disableScroll(true);
      }
      if (window.StatusBar) {
        StatusBar.styleDefault();
      }
    });
  })

  .config(function($stateProvider, $urlRouterProvider, $translateProvider) {
    $translateProvider.translations('es', {
      etiqueta_nombre_de_usuario: "Nombre de usuario",
      boton_buscar: "Buscar",
      boton_cambiar_usuario: "Cambiar usuario",
      etiqueta_fotos: "Fotos",
      boton_ordenar_por_fecha: "Ordenar por nombre",
      boton_ordenar_por_nombre: "Ordenar por fecha",
      etiqueta_comentarios: "Comentarios",
      etiqueta_no_hay_comentarios: "No hay comentarios",
      etiqueta_albumes: "Albumes",
      etiqueta_preferencias: "Preferencias",
      etiqueta_lenguaje: "Lenguaje",
      etiqueta_español: "Español",
      etiqueta_ingles: "Inglés",
      etiqueta_temas: "Temas",
      etiqueta_nocturno: "Nocturno"
    });
    $translateProvider.translations('en', {
      etiqueta_nombre_de_usuario: "User name",
      boton_buscar: "Search",
      boton_cambiar_usuario: "Change user",
      etiqueta_fotos: "Photos",
      boton_ordenar_por_fecha: "Order by name",
      boton_ordenar_por_nombre: "Order by date",
      etiqueta_comentarios: "Comments",
      etiqueta_no_hay_comentarios: "No comments",
      etiqueta_albumes: "Albums",
      etiqueta_preferencias: "Preferences",
      etiqueta_lenguaje: "Language",
      etiqueta_español: "Spanish",
      etiqueta_ingles: "English",
      etiqueta_temas: "Themes",
      etiqueta_nocturno: "Dark side"
    });
    $translateProvider.preferredLanguage("es");
    $translateProvider.fallbackLanguage("es");

    $urlRouterProvider.otherwise('/app/home');
    $stateProvider
      .state('app',   {
        url:   '/app',
        abstract:  true,
          //IMPORTANTE!!! /app es abstracto!!! 
        templateUrl:   'templates/header.html'
      })
      .state('app.home', {
        url: '/home',
        views:  {
          'bodyContent':  {
            templateUrl: 'templates/albums.html',
            controller: 'albumsCtrl'
          }
        }
      })
      .state('app.album', {
        url: '/album/:id',
        views: {
          'bodyContent':  {
            templateUrl: 'templates/fotos.html',
            controller: 'fotosCtrl'
          }
        }
      })
  })

  .directive('userProfile', function() {
    return {
      templateUrl: 'templates/user-profile.html'
    };
  })
  .directive('albumInfo', function() {
    return {
      templateUrl: 'templates/album-info.html'
    };
  })

  .run(function verificarLenguaje($translate) {
    this.setLanguaje = function(){
      if (typeof navigator.globalization !== "undefined") {
        var lang = (navigator.language).split("-")[0];
        $translate.use(lang);
      }
    }
    document.addEventListener('deviceReady', function() {
        setLanguaje();
    }, false);
  })
  .run(function notificationPermission($ionicPopup) {
  document.addEventListener('deviceReady', function() {
    cordova.plugins.notification.local.hasPermission(function(notificationEnable) {
      if (!notificationEnable) {
        cordova.plugins.notification.local.registerPermission();
      }
    });
  }, false);

  document.addEventListener("offline", onOffline, false);
  function onOffline() {
    $ionicPopup.alert({
      title: 'Sin conexión',
      template: 'Solo podrá ver el perfil almacenado'
    });

    var date = new Date();
    var icoNotification = "res://ic_notif_flickdsm";
    var fullPath = "file:/resources/android/icon/notification//ic_notif_flickdsm";
    cordova.plugins.notification.local.schedule({
      id: 1,
      title: "DSMflickrApp",
      message: "Verifique su conexión y vuelva a intentarlo",
      at: date,
      icon: fullPath,
      sound: null
    });
  }
})
