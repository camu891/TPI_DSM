angular.module('DSMflickrApp')
.value('Flickr_data', {
  key: '&api_key=4fd46308e23127a39666782ed1be4032',
  endpoint: 'https://api.flickr.com/services/rest/?method=',
  format_json: '&format=json&nojsoncallback=1',
  user_id_method: 'flickr.people.findByUsername',
  user_albums_method: 'flickr.photosets.getList',
  photosets_id_method: 'flickr.photosets.getPhotos',
  photo_size_method: 'flickr.photos.getSizes',
  photo_info_method: 'flickr.photos.getInfo',
  user_data_method: 'flickr.people.getInfo',
  comment_data_method: 'flickr.photos.comments.getList',
  user_name_need: '&username=',
  user_id_need: '&user_id=',
  photoset_id_need: '&photoset_id=',
  photo_id_need: '&photo_id=',
  secret_need: '&secret='

})
.service('flickrAlbumService', function($q, $http, Flickr_data) {

  // Getting List of Photoset in a user account.
  this.getUserData = function(user_id) {
    var url = this.createUri(Flickr_data.user_data_method, Flickr_data.user_id_need, user_id);
    return $http.get(url).then(function(respuesta) {
      return _.cloneDeep(respuesta);
    });
  }

  this.getUserId = function(user_name) {
    var url = this.createUri(Flickr_data.user_id_method, Flickr_data.user_name_need, user_name);
    return $http.get(url).then(function(respuesta) {
      return _.cloneDeep(respuesta);
    });
  }

  this.getPhotoSets = function(user_id) {
    var url = this.createUri(Flickr_data.user_albums_method, Flickr_data.user_id_need, user_id)
    return $http.get(url).then(function(respuesta) {
      return _.cloneDeep(respuesta);
    });
  }

  this.getAlbumPhotos = function(album_id) {
    var url = this.createUri(Flickr_data.photosets_id_method, Flickr_data.photoset_id_need, album_id)
    return $http.get(url).then(function(respuesta) {
      return _.cloneDeep(respuesta);
    });
  }

  this.getPhotoInfo = function(photo_id, secret) {
    url_sizes = Flickr_data.endpoint + Flickr_data.photo_size_method +
    Flickr_data.key +
    Flickr_data.photo_id_need + photo_id + Flickr_data.format_json;

    url_info = Flickr_data.endpoint + Flickr_data.photo_info_method +
    Flickr_data.key +
    Flickr_data.photo_id_need + photo_id +
    Flickr_data.secret_need + secret +
    Flickr_data.format_json;

    return $q.all([
      $http.get(url_sizes),
      $http.get(url_info)
    ]).then(function(respuesta) {
      return _.cloneDeep(respuesta);
    });
  }

  this.getPhotoComments = function(photo_id) {
    var url = this.createUri(Flickr_data.comment_data_method, Flickr_data.photo_id_need, photo_id);
    return $http.get(url).then(function(respuesta) {
      return _.cloneDeep(respuesta);
    });
  }

  this.createUri = function(method, need, need_value) {
    var url = Flickr_data.endpoint + method + Flickr_data.key +
    need + need_value + Flickr_data.format_json;
    return url;
  }

});
