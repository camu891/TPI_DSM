angular.module('DSMflickrApp')
.controller('albumsCtrl', function($scope, flickrAlbumService, $ionicLoading, $rootScope, $ionicPopup, persistenceService, $translate, $ionicModal) {
  $scope.leng = navigator.language;
  $scope.flagChangeUser = false;
  $scope.errorBusqueda = false;
  $scope.albumList = [];
  $scope.countAlbums = $scope.albumList.length;
  $scope.dark = false;

  var storage = window.localStorage;

  $scope.darkOn = function(d) {
    if (d) {
      $scope.dark = true;
    } else {
      $scope.dark = false;
    }
  }

  $ionicModal.fromTemplateUrl('templates/modal-preferences.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  var alertPopup = function(title, desc) {
    $ionicPopup.alert({
      title: title,
      template: desc
    });
  }

  $scope.changeLanguage = function(key) {
    $translate.use(key);
  };

  $scope.getAlbumList = function() {
    persistenceService.getAlbumList().then(function(result) {
      $scope.albumList = result;
      console.log("saving action DONE");
    });
  }

  $scope.saveAlbumList = function() {
    persistenceService.saveAlbumList($scope.albumList).then(function(result) {
      console.log("saving action DONE");
    });
  }

  $scope.buscar = function(user_name) {
    var networkState = navigator.connection.type;
    if(networkState === Connection.NONE){
      alertPopup('Sin conexión', "Solo puede ver los datos almacenados");
      return;
    }

    if (user_name === undefined || user_name == "") {
      alertPopup('Algo hiciste mal', 'Complete el campo');
      return;
    }

    $scope.flagChangeUser = false;
    $ionicLoading.show();
    flickrAlbumService.getUserId(user_name).then(function(result) {
      try {
        var user_id = result.data.user.id;
      } catch (e) {
        alertPopup('Algo hiciste mal', 'Ese usuario no existe');
        $scope.changeUser();
        return;
      } finally {
        $ionicLoading.hide();
      }

      flickrAlbumService.getUserData(user_id).then(function(result) {

        $scope.user_data = result.data.person;
        var iconFarm = $scope.user_data.iconfarm;
        var iconServer = $scope.user_data.iconserver;
        var nsid = $scope.user_data.nsid;
        var url = 'http://farm' + iconFarm + '.staticflickr.com/' + iconServer + '/buddyicons/' + nsid + '.jpg';
        //test userLogo
        fetch(url).then(function() {

          $scope.user_data.urlprofile = url;
          $rootScope.urlprofile = url;
        }).catch(function() {
          console.log("error");
          url = 'img/userImg.png';
          $scope.user_data.urlprofile = url;
          $rootScope.urlprofile = url;
        });
      }).then(function() {

        flickrAlbumService.getPhotoSets(user_id).then(function(result) {
          var albums = result.data.photosets.photoset;
          $scope.countAlbums = albums.length;

          albums.forEach(function(entry) {
            var resultado = flickrAlbumService.getAlbumPhotos(entry.id).then(function(resultado) {
              // console.log(resultado.data.photoset.photo[0]);
              var photo = resultado.data.photoset.photo[0];
              var farm = photo.farm;
              var server = photo.server;
              var id = photo.id;
              var secret = photo.secret;
              var url = '  https://farm' + farm + '.staticflickr.com/' + server + '/' + id + '_' + secret + '.jpg'
              entry.photoalbumurl = url;
              $ionicLoading.hide();
            });
          });
          $scope.albumList = albums;

          persistenceService.saveUserData($scope.user_data, $scope.albumList);
        })
      })
    })
  };

  //Cuando no hay eonexion $scope.buscar debe traer lo que hay almacenado
  document.addEventListener("offline",function() {
    if(persistenceService.getUserData() !== 'undefined') {
      $scope.user_data = persistenceService.getUserData();
      $scope.albumList = persistenceService.getAlbumList();
      url = 'img/userImg.png';
      $scope.user_data.urlprofile = url;
      $rootScope.urlprofile = url;
    } else {
      alertPopup('Sin conexión', 'No hay ningun perfil almacenado');
    }
  }, false);

  $scope.changeUser = function() {
    if ($scope.flagChangeUser == false) {
      $scope.flagChangeUser = true;
    } else {
      $scope.flagChangeUser = false;
    }
  }

  if(navigator.connection.type !== Connection.NONE){
    $scope.buscar('WideEyedIlluminations');
  }
});
