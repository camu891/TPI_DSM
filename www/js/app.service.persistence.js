angular.module('DSMflickrApp')
.service('persistenceService', function($q, $http) {
  var storage = window.localStorage;

  this.saveUserData = function(userData, albumList){
    storage.setItem("userData", JSON.stringify(userData));
    storage.setItem("albums", JSON.stringify(albumList));
  }

  this.getUserData = function(userData){
    var userData = JSON.parse(storage.getItem("userData"));
    return userData;
  }

  this.existUserData = function (userId){
    var userData = JSON.parse(storage.getItem("userData"));
    return userData.id == userId;
  }

  this.savePhotoList = function(photoList){
    storage.setItem("lastPhotos", JSON.stringify(photoList));
    return "DONE";
  }

  this.getPhotoList = function(){
    var photoList = JSON.parse(storage.getItem("lastPhotos"));
    return photoList;
  }

  this.getAlbumList = function(){
    var albumList = JSON.parse(storage.getItem("albums"));
    return albumList;
  }

  this.existPhotoList = function(photoListId){
    return false;
  }

  this.saveAlbumInfo = function(albumInfo){
    storage.setItem("albumInfo", JSON.stringify(albumInfo));
    return "DONE";
  }

  this.getALbumInfo = function(){
    var albumInfo = JSON.parse(storage.getItem("albumInfo"));
    return albumInfo;
  }

});
