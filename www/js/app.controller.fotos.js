angular.module('DSMflickrApp')
.controller('fotosCtrl', function($stateParams, $scope, flickrAlbumService, $ionicLoading, $rootScope, persistenceService) {

  $scope.album_id = $stateParams.id;
  $scope.photoList = [];
  $scope.commentList = [];
  $scope.order = "info.photo.dates.taken";

  var flagComments = false;

  $scope.savePhotoList = function() {
    persistenceService.savePhotoList($scope.photoList).then(function(result) {
      console.log("saving action DONE");
    });
  }

  $scope.getPhotoList = function() {
    persistenceService.getPhotoList().then(function(result) {
      $scope.photoList = result;
      console.log("getting action DONE");
    });
  }

  $scope.share = function(url, name) {
    var sub = "Hey! Mira esta foto de " + name + " en Flickr";
    window.plugins.socialsharing.shareViaEmail(url, sub, '');
  }

  $scope.hideComments = function() {
    $scope.idPhoto = "";
    flagComments = false;
  }

  $scope.openBrowser = function(url) {
    window.open(url, '_system');
  }

  $scope.buscarComentario = function(photoId) {
    $scope.flagLoading = true;
    $scope.commentList = [];

    $scope.noComment = false;
    flagComments = true;

    if (flagComments == true & $scope.idPhoto == photoId) {
      $scope.idPhoto = "";
      flagComments = false;
      return;
    }
    $scope.idPhoto = photoId;

    flickrAlbumService.getPhotoComments(photoId).then(function(result) {
      $scope.commentList = [];
      var comment = result.data.comments.comment;
      $scope.flagLoading = false;
      if (typeof comment !== "undefined") {
        comment.forEach(function(entry) {
          entry.datecreate = new Date(entry.datecreate * 1000);
          $scope.commentList.push(entry);
        });
      } else {
        $scope.noComment = true;
      }
    });
  }

  $scope.cargarDatos = function (result){
    $scope.albumName = result.data.photoset.title;
    $scope.countPhotos = result.data.photoset.total;
    $scope.ownerName = result.data.photoset.ownername;
    $scope.photos = result.data.photoset.photo;
    $scope.title = result.data.photoset.title;
  }


  var networkState = navigator.connection.type;

  if(networkState !== Connection.NONE){
    $ionicLoading.show();
    flickrAlbumService.getAlbumPhotos($scope.album_id).then(function(result) {

      persistenceService.saveAlbumInfo(result);
      $scope.cargarDatos(result);

      var promises = [];

      angular.forEach($scope.photos, function(photo) {
        var id = photo.id;
        var secret = photo.secret;
        promises.push(
          flickrAlbumService.getPhotoInfo(id, secret).then(function(result) {
            $scope.photoList.push({
              sizes: result[0].data,
              info: result[1].data
            });
            $ionicLoading.hide();
          })
        );
      });
      Promise.all(promises).then(function(){
        persistenceService.savePhotoList($scope.photoList);

      })
    })
  } else {
    $scope.photoList = persistenceService.getPhotoList();
    var result = persistenceService.getALbumInfo();
    $scope.cargarDatos(result);
  }
})
